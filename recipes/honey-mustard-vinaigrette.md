---
title: Honey Mustard Vinaigrette
published: 2021-01-02
tags:
 - Salad Dressing
 - Condiment
ingredients:
 - 4 Tbs olive oil
 - 3 tbs honey
 - 1 Tbs apple cider vinegar
 - 3-4 tsp dijon mustard
directions:
 - Combine all ingredients in a bowl
 - Wisk well until combined

Mustard will help the oil & vinegar mixutre emulsify
