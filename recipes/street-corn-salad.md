---
title: Street Corn Salad
published: 2021-01-02
tags:
 - Summer
 - Fall
 - Side Dish
ingredients:
 - 6 ears of Corn
 - 3 Tbs Lime Juice (~2 limes)
 - 3 Tbs Sour Cream
 - 1 Tbs Greek Yogurt
 - ~2 Serrano Chilis Seeded & cut into 1/8 rings
 - 2 Tbs Vegetable Oil
 - 1/2 Tsp Chili Powder
 - 4oz Cotija Chese
 - 3 scallions, thinly sliced
 - Salt to taste
directions:
 - Combine lime juice, sour cream, greek yogurt, serranos & 1/r tsp salt in a large bowl. Set aside.
 - Heat 1Tbs oil in large nonstick skillet over high heat until shimmering. Add half the corn (even layer). Sprink;e w/ dash of salt. Cover & cook, without stirring, util charred (~3 minutes). Remove from heat and let stand, covered, for 15 seconds. Transfer to bowl with sour cream mixture and repeat with remaining corn...OR GRILL THE CORN!!! EASIER & YUMMIER!!!
 - Return Skillet to medium heat. add 1tsp oil & chili power Cook stirring constantly, until fragrant (~30 seconds). Transfer to bowl with corn & toss to coat. Let cool for 15 mins.
 - Add cotija, cilantro, and scallions and toss to coat. Season with salt & lime juice.

From my cooks illustrated
