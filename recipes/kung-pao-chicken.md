---
title: Kung Pao Chicken
published: 2021-01-02
tags:
 - Main
ingredients:
 - 2 Tbs Peanut Oil
 - 1 Lb boneless skinless chicken breast cut into cubes
 - 2 bell peppers (1 red & 1 green) diced in large pieces
 - 1/2 cup peanuts
 - 4 dried arbol chili peppers (seeds removed & finely chopped)
 - 1/2 cup water
 - 6 Tbs soy sauce
 - 4 Tbs rice wine winegar
 - 2 Tbs cornstarch
 - 2 Tbs Honey
 - 2 Tsp Sesame Oil
 - 1/2 Tsp Ground Ginger
 - 1/4 Tsp White Pepper (or black if you dont have it)
directions:
 - Whisk together water, soy sauce, rice wine vinegar, cornstarch, honey, sesame oil, ginger, & pepper in a bowl. Set aside
 - Heat 1 Tbs Peanut Oil in awok or large saute pan over medium-high heat
 - Add diced checken breast pieces and saute for 5 minutes flipping occassionally. Transfer cooked chicken to a plate and set aside
 - Add remaining peanut oil to wok, then stir in bell peppers, peanuts, and chopped chili peppers. Saute for 3-4 minutes until soft
 - Stir in marinade and let simmer until thickened
 - Add cooked chicken and stir to combine
 - Service over rice

Found on Pinterest
