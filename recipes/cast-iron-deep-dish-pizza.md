---
title: Cast Iron Deep Dish Pizza
published: 2021-01-02
tags:
 - Main
equipmenet:
 - 12" cast iron skillet
ingredients:
 - Pizza Dough
 - Pizza Sauce
 - 4 oz Montrey Jack
 - Toppings
 - 7 oz Mozzeralla Shredded
 - Olive Oil
directions:
 - Preheat the oven to 400F
 - Coat the bottom of a 12" cast iron skillet w/ Olive Oil
 - Place Dough into killet & flatten with fingers until it reaches the edges of the skillet
 - Let dough rest in skillet @ room temperature for at least 30 minutes but up to 90 minutes
 - Surround the edges of the pizza w/ Montrey Jack cheese pushing it down the sides. (This will form a crispy crust around the edges)
 - Finish regular pizza making
 - Cook in oven for 25-30 minutes
 - After removing from the oven let sit for 30 minutes before running a butter knife along the side to check the bottom
 - If bottom is not well browned cook on stoetop over medium heat until doneness is achieved (this can happen quickly keep an eye on it)
