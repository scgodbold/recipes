---
title: Bacon Wrapped Dates
published: 2021-01-02
tags:
 - Holiday
 - Appetizer
ingredients:
 - 8 slices of thin bacon
 - 16 dates
 - 4oz goat cheese
 - Toothpicks
directions:
 - Preheat oven to 350F
 - Slice dates lengthwise on one side and remove pit
 - Stuff a small amount of goat cheese into the cavity of each date and press sides together to close
 - Cut bacon in half. Wrap each date with a piece of bacon and secure with a toothpick
 - Arrange on a baking sheet with raised edges )so there's no great spillage) and bake for 10 minutes
 - Remove dates and use toothpick to flip them onto the other side. Bake another 5-8 minutes (or until crispy)
 - Let stand on paper towel lined plate 5 minutes before serving

Found on pinterest
