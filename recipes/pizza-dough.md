---
title: Pizza Dough
published: 2021-01-02
tags:
 - Bread
 - Component
ingredients:
 - 1.5 cups warm water
 - 2 packets (1/4 oz each) active dry yeast
 - 2 Tsp surgar
 - 1/4 Cup Extra Virgin Olive Oil, plus some extra
 - 1 Tsp salt
 - 4 Cups AP Flour
directions:
 - Pour water into large bowl; sprinkle with yeast and sugar. Let stand until foamy, about 5 minutes
 - Brush another large bowl with olive oil
 - When yeast is foamy, whish in oil and salt, Add Flour (1 cup at a time) and stire until a sticky dough forms
 - Knead until it reaches a smooth texture
 - Put dough in oiled bowl(drizzle more oil on top. Cover with plastic wrap and set aside until dough has doubled in size (~1 hour).
 - Once risen turn out onto floured surface knead a few times forming a round.
 - Let rest for 10 minutes.
 - Use now or store in fridge for upto a week or freezer for a month

A stand mixer makes bringing the dough together much easier. Use a dough hook
