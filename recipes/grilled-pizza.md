---
title: Grilled Pizza
published: 2021-01-02
tags:
 - Main
 - Summer
 - Grilling
ingredients:
 - Pizza Dough
 - Toppings of your choosing
 - Olive Oil
directions:
 - Make your pizza dough
 - Make sure th grill is cleand & brushed w/ olive oil. Preheat to medium heat.
 - Once dough is risen, divide into 4-8 pieces depending on the size you like. Roll the dough out.
 - Brush top of pizza w/ olive oil, then place pizzas on grill, oiled side down
 - Cover and cook ~5 minutes until bottom is golden brown
 - Remove pizza onto a plate, brush the uncooked side w/ Olive Oil then place back onto the grill cooked side up
 - Add the toppings to the pizza
 - Cover & cook until cheese is melted
 - Let sit for a few minutes then eat

This is very grill specific and will probably take a little expirmenting to get perfect.

[Pizza Dough Recipe](/recipe/pizza-dough/)
