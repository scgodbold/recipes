---
title: Spinach Artichoke Dip
published: 2021-01-02
tags:
 - Appetizer
 - Dip
ingredients:
 - 8 oz Cream Cheese
 - 16 oz light Sour Cream
 - 8 Tbs Unsalted Butter
 - 1.5 Cups Shredded Parmesan Cheese
 - 14 oz quartered artichoke hearts (drained & chopped)
 - 4 oz can diced Jalapenos (drained)
 - 10oz Chopped Spinach
directions:
 - In medium pot over medium heat, melt together cream cheese, sour cream, butter, and parmesan cheese
 - Stir frequently until melted, bubbly and an even consistency
 - Stir in artichoke hearts, drained jalapenos, and coarsly chopped spinach. It maay take several batches to succesfully wilt in all the spinach
 - Serve hot with chips, crackers or toasted baguettes

We have had success substituting half the butter for olive oil, though it will split slowly over time and need to get stired back in.

Found on Pinterest.

