---
title: Fridge Dill Pickles
published: 2021-01-02
tags:
 - Canning
 - Staple
 - Condiment
ingredients:
 - 4 Lbs Pickling Cucumbers
 - 3 Cups White Vinegar
 - 4 Cups Water
 - 1/4 cup Pickling or Kosher Salt
 - 12 fresh dill sprigs
 - 6 tsp dill seed
 - 3 tsp Black Peppercorns
 - 1.5 tsp Red Chili Flakes
directions:
 - Wash cucumbers and slice into 1/4 inch coins (or quarter lengthwise for spears)
 - Add vinegar, water, and salt to medium saucepan. Bring to a boil over high heat, stirring until salt is dissolved. Lower and cover until ready to us
 - At the bottom of each mason jar (wash them first though you heathen!) add a sprig of dill, 1 tsp dill seed, 1/2 tsp black peppercorns, 1/4 tsp red chili flakes
 - Add cucumbers (pack semi-tightly). Add another dill sprig to the top. Ladle hot viegar mixture into jars, leaving 1/4 inch at the top. Clean rim of jars with a wet paper towel and screw on lids.
 - Put jars in refrigerator and allow to arinate for at least 3 weeks for best flavor. They'll be good for a few months

You may also can these pickles but we are lazy and impatient so the fridge version works for us.

If you like sweeter pickles you may add sugar to the pickling liquid as well.
