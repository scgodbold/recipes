---
title: Nana's Vegetable Beef Barley Soup
published: 2021-01-02
tags:
 - Soup
 - Fall
 - Winter
 - Beef
ingredients:
 - 1.5 lbs. beef or chuck (any kind of beef works really)
 - 1 Tsp Salt
 - 1/2 Tsp Black Pepper
 - 4 Bay Leaves (you can never have to many bay leaves)
 - 3 or 4 carrots cut into coins
 - Pinch of oregano
 - 1 can (28 oz) Crushed tomatoes
 - 1 can (14 oz) Diced tomatoes, drained
 - 1 Tsp Worcestershire sauce
 - 28 oz Beef Broth (she used boullion cubes, which I cant eat)
 - 1 onion chopped
 - 3 or 4 celery stalks chopped
 - 1 cup chopped cabbage (napa, chinese, or regular)
 - 1/2 cup barley (or pasta) rinsed well
directions:
 - Cover meat with broth in a heavy kettle. Add salt, pepper and bay leaves and let it come to bubbling stage
 - Turn the heat low and add in all remaining ingredients except the barley
 - Simmer at least 3 hours or until tender
 - Remove the bay leaves & meat
 - Throw away the bay leaves & cut the meat into bite sized pieces
 - Put meat back into the pot and add the barley. Simmer for another hour until barley is tender
 - Serve (if it seems to thick at this point you may add a bit more water in)

One of Nana's favorites, given to me Christmas of 2020. I have included the inscription that came along with it


> Dear Scott, I know that you like soup (as I do) so I thought I would share one of my favorite soup recipes and the indredients with you. When I was first married (1960), I was looking through a amagaine and saw this recipe of Hubert Humphrey's. I have included many of the ingredients with the hopes that you will make and enjoy this soup over the winter. Since I could not include the fresh vegetables and meat I have attached $20. I hope you enjoy this soup as much as I have over the years!
>
> \- Nana

