---
title: Fall Salad
published: 2021-01-02
tags:
 - Salad
 - Fall
ingredients:
 - 4-6 cups spinach or mixed greens
 - 1.2 cup dried cranberries
 - 1/2 cup crushed walnuts
 - 1/3 cup crubled feta cheese
 - 1/2 sliced  granny smith apple
 - 1/4 sliced red onion
 - 4 sliceds of bacon cooked and diced
directions:
 - Combine ingredients in a bowl and dress to taste

We usually use a [honey mustard vinaigrette](/recipe/honey-mustard-vinaigrette/)
