---
title: Cranberry Relish
published: 2021-01-02
tags:
 - Holiday
 - Side Dish
 - Winter
equipment:
 - Food Processor
ingredients:
 - 12oz Cranberries
 - 1 Navel Orange
 - 1/8 Tsp Cinnamon
 - 1/2 Cup white sugar
directions:
 - Rinse Cranberries and add them to the food pprocessor with sugar & cinnamon
 - Zest orange into the food processor
 - Remove the remaining orange pith and put the orange slices into the food processor
 - Run until all large chunks are broken down & relish consistency is reached

From my mother, passed on from my grandmother.
