---
title: Fridge Pickled Jalapenos
published: 2021-01-02
tags:
 - Canning
 - Staple
 - Condiment
ingredients:
 - 1 Lbs Jalapenos (stems removed) thinly sliced
 - 1 Cups White Vinegar
 - 1 Cups Water
 - 1/4 cup Granulated Sugar
 - 2 Tbs Kosher Salt
directions:
 - Wash jalapnoes and slice into 1/4 inch coins
 - Add vinegar, water, sugar, and salt to medium saucepan. Bring to a boil over high heat, stirring until salt and sugar is dissolved
 - Once boilingm add the sliced jalapenos pressing them so the are submerged in the pickling liquid
 - Remove the pot from the heat and let sit for 10-15 minutes, stirring occasisionally
 - Transfer to 2 pint jars or 1 quart jar. Ladle the prine over the top until the jar is full. Let them cool to room temperature before securing lids and putting them in the fridge.

The sugar in this recipe takes the heat off the pepers. So increase the sugar if to hot or decrease if to mild for your taste.
