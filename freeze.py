import app
from flask_frozen import Freezer

freezer = Freezer(app.app)

@freezer.register_generator
def tag():
    tags = []
    for p in app.pages:
        for t in p.meta['tags']:
            if t not in tags:
                tags.append(t)
    for t in tags:
        yield {'tag': t}

@freezer.register_generator
def recipe():
    for p in app.pages:
        yield {'recipe': p.path}

if __name__ == '__main__':
    freezer.freeze()
