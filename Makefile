.PHONY: all

all: build

build: venv
	./venv/bin/python freeze.py

venv:
	python3 -m venv venv
	./venv/bin/pip install -r requirements.txt

clean-venv:
	rm -rf ./venv

rebuild-venv: clean-venv venv

run: venv
	./venv/bin/python run.py
