import os.path
from flask import Flask, render_template
from flask_flatpages import FlatPages
import pyavagen

import config

app = Flask(__name__)
app.config.from_object(config.Configuration)
pages = FlatPages(app)

@app.route('/')
def index():
    recipes = (p for p in pages)
    for p in pages:
        img = 'static/img/{}.png'.format(p.path);
        if not os.path.exists(img):
            # Create a garbage avatar for recipes without one
            avatar = pyavagen.Avatar(pyavagen.SQUARE_AVATAR, size=500)
            avatar.generate().save(img)
    return render_template('layouts/preview.html', pages=pages)

@app.route('/tags/')
def tags():
    tags = {}
    for p in pages:
        for t in p.meta['tags']:
            if t not in tags:
                tags[t] = [p]
            else:
                tags[t].append(p)
    return render_template('layouts/tags.html', tags=tags)

@app.route('/tag/<tag>/')
def tag(tag):
    recipes = [p for p in pages if tag in p.meta['tags']]
    return render_template('layouts/preview.html', pages=recipes)

@app.route('/recipe/<recipe>/')
def recipe(recipe):
    r = pages.get_or_404(recipe)
    return render_template('layouts/recipe.html', recipe=r)
