class Configuration(object):
    DEBUG = True
    FLATPAGES_ROOT = 'recipes'
    FLATPAGES_EXTENSION = '.md'
    FREEZER_DESTINATION = 'nginx/www'
    FREEZER_BASE_URL = 'https://recipes.scgodbold.com'
